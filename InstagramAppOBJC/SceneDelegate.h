//
//  SceneDelegate.h
//  InstagramAppOBJC
//
//  Created by Kaloyan on 8.07.22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

